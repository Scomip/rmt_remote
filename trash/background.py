from threading import Thread

import time

from rmt.trashcan import TrashRecord

from .models import TaskTarget, TaskTargetError, Trashcan


class BackgroundTask(Thread):
    period = 1

    def __init__(self):
        super(BackgroundTask, self).__init__()
        self.daemon = True

    def _run_task(self):
        pass

    def run(self):
        while True:
            self._run_task()
            time.sleep(self.period)


class Fetcher(BackgroundTask):
    period = 2

    def _fetch(self, trashcan):
        while True:
            fetch_result = trashcan.instance.fetch()
            if fetch_result is None:
                break
            action_id, result = fetch_result

            task_target = TaskTarget.objects.get(
                trashcan=trashcan,
                action_id=action_id
            )

            if isinstance(result, TrashRecord):
                task_target.status = TaskTarget.STATUS_SUCCESS
            else:
                task_target.status = TaskTarget.STATUS_FAIL
                task_target.error = TaskTargetError.objects.create(
                    reason=str(result)
                )

            task_target.save()

    def _run_task(self):
        for trashcan in Trashcan.objects.all():
            self._fetch(trashcan)


class Saver(BackgroundTask):
    period = 5

    def _run_task(self):
        for trashcan in Trashcan.objects.all():
            trashcan.instance.save()
