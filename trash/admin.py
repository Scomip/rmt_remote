# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Trashcan, Task, TaskTarget, TaskTargetError


class TrashcanAdmin(admin.ModelAdmin):
    fields = ['name', 'path']


class TaskAdmin(admin.ModelAdmin):
    fields = ['trashcan', 'status']


class TaskTargetAdmin(admin.ModelAdmin):
    fields = ['task', 'target', 'status', 'error']


class TaskTargetErrorAdmin(admin.ModelAdmin):
    fields = ['reason']


admin.site.register(Trashcan, TrashcanAdmin)
admin.site.register(Task, TaskAdmin)
admin.site.register(TaskTarget, TaskTargetAdmin)
admin.site.register(TaskTargetError, TaskTargetErrorAdmin)
