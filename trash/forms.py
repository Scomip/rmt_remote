from django import forms

from .models import Trashcan


class CreateTrashcanForm(forms.ModelForm):
    class Meta:
        model = Trashcan
        fields = ('name', 'path')


class PutTaskForm(forms.Form):
    targets = forms.CharField(max_length=10000, widget=forms.Textarea)
