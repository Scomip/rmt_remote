function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

var selectedItems = new Set()

$('#contentsTable').on('click', '.clickable-row', function (event) {
    var id = $(this).find('.id-cell').html();
    if (selectedItems.has(id)) {
        $(this).removeClass('active');
        selectedItems.delete(id);
    } else {
        $(this).addClass('active');
        selectedItems.add(id);
    }
});

$('#eraseButton').on('click', function () {
    $.ajax({
        type: 'POST',
        url: '/ajax/erase',
        data: {
            trashcan_id: currentTrashcanId,
            targets: Array.from(selectedItems),
            csrfmiddlewaretoken: csrftoken
        },
        dataType: 'json',
        success: function(data) {
            if (data.status == 'success') {
                window.location.replace(data.redirect);
            }
        }
    });
});

$('#restoreButton').on('click', function () {
    $.ajax({
        type: 'POST',
        url: '/ajax/restore',
        data: {
            trashcan_id: currentTrashcanId,
            targets: Array.from(selectedItems),
            csrfmiddlewaretoken: csrftoken
        },
        dataType: 'json',
        success: function(data) {
            if (data.status == 'success') {
                window.location.replace(data.redirect);
            }
        }
    });
});
