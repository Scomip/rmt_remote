# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import rmt_async.trashcan

from django.db import models
from threading import Lock


class Trashcan(models.Model):
    _instances = {}
    _instances_lock = Lock()

    name = models.CharField(max_length=100, unique=True, null=False)
    path = models.CharField(max_length=1000)
    next_action_id = models.IntegerField(default=1)

    @property
    def items(self):
        return list(self.instance.items())

    @property
    def instance(self):
        with self._instances_lock:
            if self.id not in self._instances:
                instance = rmt_async.trashcan.AsyncTrashcan(
                    self.path,
                    initial_action_id=self.next_action_id
                )
                self._instances[self.id] = instance
            return self._instances[self.id]

    def __str__(self):
        return self.name

    def create_task(self, task_type, targets):
        task = Task.objects.create(
            trashcan=self,
            task_type=task_type,
            status=Task.STATUS_DRAFT,
        )
        for record_id in targets:
            TaskTarget.objects.create(
                task=task,
                trashcan=self,
                target=record_id,
                status=TaskTarget.STATUS_DRAFT
            )
        task.run()
        return task


class TaskTargetError(models.Model):
    reason = models.CharField(max_length=300)

    def __str__(self):
        return self.reason


class TaskTarget(models.Model):
    STATUS_SUCCESS = 0
    STATUS_FAIL = 1
    STATUS_DRAFT = 2
    STATUS = (
        (STATUS_SUCCESS, 'Success'),
        (STATUS_FAIL, 'Fail'),
        (STATUS_DRAFT, 'Draft'),
    )

    task = models.ForeignKey('Task')
    trashcan = models.ForeignKey('Trashcan')
    target = models.CharField(max_length=100)
    status = models.IntegerField(choices=STATUS)
    action_id = models.IntegerField(null=True)
    error = models.ForeignKey('TaskTargetError', null=True)


class Task(models.Model):
    STATUS_SUCCESS = 0
    STATUS_FAIL = 1
    STATUS_DRAFT = 2
    STATUS_EXECUTING = 3
    STATUS = (
        (STATUS_SUCCESS, 'Success'),
        (STATUS_FAIL, 'Fail'),
        (STATUS_DRAFT, 'Draft'),
        (STATUS_EXECUTING, 'Executing'),
    )

    TASK_TYPE_PUT = 0
    TASK_TYPE_ERASE = 1
    TASK_TYPE_RESTORE = 2

    TASK_TYPE = {
        TASK_TYPE_PUT: 'Put',
        TASK_TYPE_ERASE: 'Erase',
        TASK_TYPE_RESTORE: 'Restore',
    }

    TASK_METHODS = {
        TASK_TYPE_PUT: rmt_async.AsyncTrashcan.put,
        TASK_TYPE_ERASE: rmt_async.AsyncTrashcan.erase,
        TASK_TYPE_RESTORE: rmt_async.AsyncTrashcan.restore,
    }

    task_id = models.AutoField(primary_key=True)
    task_type = models.IntegerField()
    trashcan = models.ForeignKey('Trashcan', on_delete=models.CASCADE)
    status = models.IntegerField()

    def __str__(self):
        return '{} :: {}'.format(self.TASK_TYPE[self.task_type], self.trashcan.name)

    def run(self):
        for task_target in TaskTarget.objects.filter(task=self):
            try:
                target = (
                    task_target.target
                    if self.task_type == Task.TASK_TYPE_PUT
                    else int(task_target.target)
                )
                action_id = self.TASK_METHODS[self.task_type](
                    self.trashcan.instance,
                    target
                )
                task_target.action_id = action_id
                self.trashcan.next_action_id += 1
            except Exception as e:
                task_target.status = TaskTarget.STATUS_FAIL
                task_target.error = TaskTargetError.objects.create(
                    reason=str(e)
                )
            else:
                task_target.status = TaskTarget.STATUS_DRAFT
                task_target.save()

        self.trashcan.save()
        self.save()
