from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.trashcan_view, {'name': None}, name='index'),
    url(r'^newcan$', views.CreateTrashcanView.as_view(), name='newcan'),
    url(r'^trashcan/$', views.trashcan_view, {'name': None}, name='trashcan'),
    url(r'^trashcan/(?P<name>\w+)/$', views.trashcan_view, name='trashcan'),
    url(r'^task/$', views.task_view, {'task_id': None}, name='task'),
    url(r'^task/(?P<task_id>\d+)$', views.task_view, name='task'),

    url(r'^put$', views.put, name='put'),
    url(r'^ajax/erase$', views.ajax_erase),
    url(r'^ajax/restore$', views.ajax_restore),
]
