# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class TrashConfig(AppConfig):
    name = 'trash'

    def ready(self):
        from .background import Fetcher, Saver
        Fetcher().start()
        Saver().start()
