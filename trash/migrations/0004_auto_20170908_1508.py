# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-08 15:08
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('trash', '0003_trashcan_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='TaskTargetError',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reason', models.CharField(max_length=300)),
            ],
        ),
        migrations.RenameField(
            model_name='tasktarget',
            old_name='path',
            new_name='target',
        ),
        migrations.AddField(
            model_name='task',
            name='task_type',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='tasktarget',
            name='action_id',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='tasktarget',
            name='error',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='trash.TaskTargetError'),
        ),
    ]
