# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.edit import FormView

from .forms import CreateTrashcanForm, PutTaskForm
from .models import Trashcan, TaskTarget, Task


class CreateTrashcanView(FormView):
    form_class = CreateTrashcanForm
    success_url = '/'

    def form_valid(self, form):
        Trashcan.objects.create(
            name=form.data.get('name'),
            path=form.data.get('path'),
        )
        return super(CreateTrashcanView, self).form_valid(form)


def put(request):
    trashcan = Trashcan.objects.get(id=request.POST.get('trashcan_id'))
    targets = [target.strip() for target in request.POST.get('targets').split('\n')]
    task = trashcan.create_task(Task.TASK_TYPE_PUT, targets)
    return redirect('task', task_id=task.task_id)


def index(request):
    return render(request, 'trash/index.html')


def trashcan_view(request, name):
    context = {
        'trashcan_form': CreateTrashcanForm(),
        'put_form': PutTaskForm(),
        'trashcans': Trashcan.objects.all()
    }
    if name is not None:
        trashcan = get_object_or_404(Trashcan, name=name)
        context['current_trashcan'] = trashcan
    return render(request, 'trash/trashcan.html', context)


def task_view(request, task_id):
    context = {
        'trashcan_form': CreateTrashcanForm(),
        'tasks': Task.objects.all()
    }

    if task_id is not None:
        current_task = get_object_or_404(Task, task_id=task_id)
        targets = TaskTarget.objects.filter(task=current_task)
        context['current_task'] = current_task
        context['targets'] = targets

    return render(request, 'trash/task.html', context)


def ajax_erase(request):
    if request.is_ajax() and request.method == 'POST':
        trashcan = Trashcan.objects.get(id=request.POST.get('trashcan_id'))
        targets = request.POST.getlist('targets[]')
        task = trashcan.create_task(Task.TASK_TYPE_ERASE, targets)
        return JsonResponse({
            'status': 'success',
            'redirect': reverse('task', kwargs={'task_id': task.task_id})
        })
    else:
        return JsonResponse({})


def ajax_restore(request):
    if request.is_ajax() and request.method == 'POST':
        trashcan = Trashcan.objects.get(id=request.POST.get('trashcan_id'))
        targets = request.POST.getlist('targets[]')
        task = trashcan.create_task(Task.TASK_TYPE_RESTORE, targets)
        return JsonResponse({
            'status': 'success',
            'redirect': reverse('task', kwargs={'task_id': task.task_id})
        })
    else:
        return JsonResponse({})
